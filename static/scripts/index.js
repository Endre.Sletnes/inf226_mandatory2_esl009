var reqId = 0;
var anchor = document.getElementById('anchor');
var messageField = document.getElementById('message');
var recvField = document.getElementById('receivers');
var sendBtn = document.getElementById('sendBtn');
var output = document.getElementById('output');
var header = document.getElementById('header');

var checkAnnouncements = async () => {
    res = await fetch('/announcements');
    anns = await res.json();
    if (anns && Array.isArray(anns.data)) {
        const elts = [];
        anns.data.forEach((element, idx) => {
            if (idx > 0) {
                const node = document.createElement('li');
                node.textContent = '  …  ';
                elts.push(node);
            }
            const node = document.createElement('li');
            node.textContent = `${element.message || ''}`;
            elts.push(node);
        });
        console.log(elts);
        header.replaceChildren(...elts);
    }
};
var send = async (message, receivers) => {
    const id = reqId++;
    const q = `/send?message=${encodeURIComponent(message)}&receivers=${encodeURIComponent(receivers)}`;
    res = await fetch(q, { method: 'post'});
    console.log(res);
    const head = document.createElement('h3');
    output.appendChild(head);
    const body = document.createElement('p');
    body.innerHTML = await res.text();
    output.appendChild(body);
    body.scrollIntoView({block: "end", inline: "nearest", behavior: "smooth"});
    anchor.scrollIntoView();
    checkAnnouncements();
};

sendBtn.addEventListener('click', () => send(messageField.value, recvField.value))
checkAnnouncements();