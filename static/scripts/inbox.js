var reqId = 0;
var searchField = document.getElementById('search');
var searchBtn = document.getElementById('searchBtn');
var seesentBtn = document.getElementById('seesentBtn');
var seerecvBtn = document.getElementById('seerecvBtn');
var allBtn = document.getElementById('allBtn');
var output = document.getElementById('output');
var anchor = document.getElementById('anchor');
var replyBtn = document.getElementById('replyBtn');

var search = async (query) => {
    const id = reqId++;
    const q = `/search?q=${encodeURIComponent(query)}`;
    res = await fetch(q);
    console.log(res);
    const head = document.createElement('h3');
    output.appendChild(head);
    const body = document.createElement('p');
    body.innerHTML = await res.text();
    output.appendChild(body);
    body.scrollIntoView({block: "end", inline: "nearest", behavior: "smooth"});
    anchor.scrollIntoView();
    checkAnnouncements();
};

var checkAnnouncements = async () => {
    res = await fetch('/announcements');
    anns = await res.json();
    if (anns && Array.isArray(anns.data)) {
        const elts = [];
        anns.data.forEach((element, idx) => {
            if (idx > 0) {
                const node = document.createElement('li');
                node.textContent = '  …  ';
                elts.push(node);
            }
            const node = document.createElement('li');
            node.textContent = `${element.message || ''}`;
            elts.push(node);
        });
        console.log(elts);
        header.replaceChildren(...elts);
    }
};

var reply = async () => {
    res = await fetch(`/chooseMsg`);
    answ = await res.json();

}

searchField.addEventListener('keydown', ev => {
    if (ev.key === 'Enter') {
        search(searchField.value);
    }
});

searchBtn.addEventListener('click', () => search(searchField.value));
allBtn.addEventListener('click', () => search('*'));
seesentBtn.addEventListener("click", () => search('seeSent'));
seerecvBtn.addEventListener("click", () => search('seeRecv'));
replyBtn.addEventListener("click", () => reply())