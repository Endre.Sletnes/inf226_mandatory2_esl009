Documentation

From the login-server I found these problems:
- Code was not organized which makes it hard to find bugs/security flaws.
  - Everything was in app.py. Moved methods to different files.
  - Both scripts and css was in the html-file, so I separated html, javascript and css. Moved them to static/scripts and static/CSS.
- Queries were shown to the user and were vulnerable to SQL-injection.
  - Removed the queries from the results.
  - Used prepared statements from apsw to prevent multiple queries injected by the user.
- The "from"-field made it possible for users to impersonate others.
  - Removed the from field
- All users could see everyone's messages.
  - Made it so users could only see messages they sent or received.
- The app.secret_key could easily be guessed.
  - Made a random key using the secrets-library.

Application features:
- Register a new user with username and password.
- Send and receive messages to/from other users.
- See all messages you have sent or received.
- Implemented a reply-page where a user would be able to reply to a message using the message-id (not done).

Instructions on demo/test:
pip installs:
 - all from login server project
 - bcrypt
 - cryptography

Register one or more new users and send messages between them either by changing accounts or having two
windows open(one might have to be incognito). Registering a user is done in the register page (quite straight forward).
All messages the user has received should show when clicking "See received" in inbox
and the messages the user has sent show up when clicking "See sent". To test just check that messages show up as they should.

Technical details:
Frameworks:
I used the same frameworks as in the login server project plus some more.
- Flask:
  - CSRFProtect from flask_wtf to protect against CSRF attacks. The forms LoginForm and RegisterForm already used a CSRF-token.
- bcrypt:
  - I used bcrypt to hash the passwords with a random salt and to check if passwords matched when logging in.
- Fernet:
  - I used Fernet from the cryptography-library to encrypt and decrypt messages in the database. I made a random key using
    Fernet.generate_key() that is stored under the app.secret_key.

- There is two databases, tiny.db which has all user and message information and applogger.db which logs suspicious activity.
  I have a list of suspicious signs, and if a message contains one of these signs it is logged in the applogger.

Questions:
- Threat model:
  I used the OWASP cheat sheet for this. (https://cheatsheetseries.owasp.org/cheatsheets/Threat_Modeling_Cheat_Sheet.html)
  - Who might attack the application?
    - Someone who would like to see another persons messages looking for confidential information.
    - Someone who would like to impersonate someone else.
    - Someone who want passwords to use on another site (maybe people use the same password elsewhere).
    - Someone who want to steal an account so the owner couldn't access it.
    - Someone who want to put the application out of service.
  - What can an attacker do?
    - Hack a users account and send messages to others impersonating that user or see that users messages.
    - Intercept a message and see or alter it.
    - Hack the database and see or alter/change messages and passwords stored.
    - Send malicious URLs to other users.
  - What damage could be done?
    - A person could gain access to messages they are not authorized for (messages between other users, etc.). This could damage
      the confidentiality of the application.
    - An attacker could change information in the database which would harm the integrity and availability (if users can't log in
      because username or password has been changed).
    - Someone could intercept messages and change them which would harm integrity.
    - Parts of the application could be put out of service which would harm the availability.
  - Limits to what attackers could do:
    - The attacker can only read messages, impersonate others or get passwords. There is no other information stored in the cookies
      like email, credit card information and so on because this site never uses it. 
      There is no users with admin access or more access than other either, so the attacker won't be able to tamper with anything
      by gaining control of other users. Therefore, the risk of being attacked is not very big.
  - Limits to what we can sensibly protect against:
    - Social engineering, if some users are a bit carefree they might press malicious links sent to them or give away their
      password. 

- Attack vectors:
  - Social engineering, phishing etc.
  - SQL Injection
  - Cross-Site Scripting
  - Man-in-the-Middle attacks
  - Cross-Site Request Forgery

- What to do against attacks?
  - To protect against SQL Injection I have used prepared statements whenever the database is accessed. apsw supports 
    prepared statements so that a user can't execute other queries alongside the intended.
  - To protect against CSRF I would have used Flask_wtf.CSRFProtect, but I couldn't get the csrf-tokens when I used 
    something else that flaskForms. My forms RegisterForm and LoginForm is still protected against CSRF attacks because they 
    use such a token.
  - To protect the application from database breach all messages and passwords are encrypted when they are in the database. 
    This does not protect from Man-in-the-Middle attacks, but it minimizes the consequences if someone got access to the database.
  - To protect against Cross-Site scripting all user inputs to the HTTP requests from the scripts to the app are encoded.
  - To protect against Man-in-the-Middle attacks we could encrypt the messages sent from the javascript and to the app.
    At the moment I only encrypt in the app.
  - To help protecting/alerting I have implemented a web application logger. This is a database with a table for logging suspicious
    messages and a table for logging when users try to log in but input wrong password or username. I could use this to see if 
    a user has tried to guess another user's password, and possibly locked the account for a time period (I have not implemented this).
    If messages contain stuff that looks malicious they are saved in this database as well for example if they contain "<>". 

- What is the access control model?
  - In my app I use login_manager for access control. Since all users have the same permissions (send message, see messages)
    the only thing I need for access control is to know who the current user is. That is handled by the login_manager and
    I can get the information by using login_manager.currentUser. 

- How can you know that your security is good enough?
  - I can do some penetration testing by trying to inject SQL or some scripts that call alert() to see if I can find some 
    vulnerabilities, or use some security testing tools.
  - Web application logging can also be used to se if there is suspicious or malicious activity and detect breaches. If 
    there is not logged any breaches (and the logging system is good) you can be pretty sure that the security is good enough
    for your application.
  
