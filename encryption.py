import bcrypt
from cryptography.fernet import Fernet


class Encryption:

    def __init__(self):
        self.secret_key = b'FeYExPhHUhNF3zIHMVMw4hgRf7qv89dJ4xR4YcGwv54='
        self.encObject = Fernet(self.secret_key)

    def encryptMsg(self, message):
        encryptedMsg = self.encObject.encrypt(message.encode("utf-8"))
        return encryptedMsg

    def decryptMsg(self, message):
        decryptedMsg = self.encObject.decrypt(message)
        return decryptedMsg.decode()

    def check_password(self, truePassword, password):
        passBytes = password.encode("utf-8")
        return bcrypt.checkpw(passBytes, truePassword)

    def hashPassword(self, password):
        passBytes = password.encode("utf-8")
        salt = bcrypt.gensalt()
        return bcrypt.hashpw(passBytes, salt)