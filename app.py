from flask import Flask, abort, request, send_from_directory, make_response, render_template
import flask
from forms.login_form import LoginForm
from forms.register_form import RegisterForm
from json import dumps
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token
from threading import local
from markupsafe import escape
from urllib.parse import urlparse, urljoin
from encryption import Encryption
import flask_login
from flask_login import login_required, login_user, logout_user


tls = local()
inject = "'; insert into messages (sender,message) values ('foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
app.secret_key = '192b9bdd22ab9ed4d12e236c78afcb9a393ec15f71bbf5dc987d54727823bcbf'

enc = Encryption()
suspiciousSigns = ["<", ">", "script", ";", "(", ")", "alert", "--", "drop", "insert"]

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
        ref_url.netloc == test_url.netloc


# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass


# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    statement = f"SELECT * FROM users WHERE username == ?;"
    u = list(conn.execute(statement, (user_id,)))
    if len(u) < 0:
        return

    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_id
    return user

def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap=True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'


@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'icons/favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'icons/favicon.png', mimetype='image/png')


@app.route('/')
@app.route('/index')
@login_required
def index():
    return send_from_directory(app.root_path,
                               'templates/index.html', mimetype='text/html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        print("validated")
        username = form.username.data
        password = form.password.data
        userCheck = 'SELECT * FROM users WHERE username == ?;'
        u = list(c.execute(userCheck, (username,)))
        if len(u) > 0:
            flask.flash("That username is already taken, please choose another one")
            return render_template('register.html', form=form)
        else:
            pwdHash = enc.hashPassword(password).decode()
            addUser = 'INSERT INTO users (username, password) VALUES (?, ?);'
            c.execute(addUser, (username, pwdHash))
            return flask.redirect(flask.url_for('index'))

    return render_template('register.html', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        sqlstatement = f"SELECT password FROM users WHERE username == ?;"
        u = list(c.execute(sqlstatement, (username,)))
        if len(u) > 0 and enc.check_password(u[0][0].encode(), password):
            user = user_loader(username)
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')

            if False and not is_safe_url(next):
                # Log what user that tried to redirect to malicious url
                loggstmt = "INSERT INTO userloggs (username, password, reason, sendtime) VALUES (?, ?, ?, DATETIME());"
                loggerc.execute(loggstmt, (username, password, "malicious url"))
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index'))
        loggstmt = "INSERT INTO userloggs (username, password, reason, sendtime) VALUES (?, ?, ?, DATETIME());"

        if len(u) > 0:
            loggerc.execute(loggstmt, (username, password, "wrong password"))
        else:
            loggerc.execute(loggstmt, (username, password, "username don't exist"))

    return render_template('./login.html', form=form)


@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return flask.redirect(flask.url_for('index'))


@app.get('/search')
def search():
    currentUser = flask_login.current_user.id
    query = request.args.get('q') or request.form.get('q') or '*'
    try:
        stmt = f"SELECT * FROM messages WHERE message GLOB ? AND (sender == ? OR reciever == ?);"
        if query == 'seeSent':
            stmt = f"SELECT * FROM messages WHERE message GLOB ? AND sender == ?;"
            c = conn.execute(stmt, ('*', currentUser))
        elif query == 'seeRecv':
            stmt = f"SELECT * FROM messages WHERE message GLOB ? AND reciever == ?;"
            c = conn.execute(stmt, ('*', currentUser))
        else:
            c = conn.execute(stmt, (query, currentUser, currentUser))
        rows = c.fetchall()
        result = 'Result:\n'
        for row in rows:
            fixedStr = f"{row[0]}, Sender: {row[1]}, Receiver: {row[2]}, Message: {enc.decryptMsg(row[3])}, " \
                       f"Timestamp: {row[4]}"
            result = f'{result}    {dumps(fixedStr)}\n'
        c.close()
        return result
    except Error as e:
        return f'{result}ERROR: {e}', 500


@app.route('/send', methods=['POST', 'GET'])
def send():
    try:
        currentUser = flask_login.current_user.id
        message = request.args.get('message')
        receivers = request.args.get('receivers')

        for i in suspiciousSigns:
            if i in message:
                loggerstmt = "INSERT INTO messageloggs (sender, receiver, message, reason, sendtime) VALUES (?, ?, ?, ?, DATETIME());"
                loggerc.execute(loggerstmt, (currentUser, receivers, message, "suspicious signs"))
                break
                print(loggerstmt)
        sqlstatement = "SELECT * FROM users WHERE username == ?;"
        u = list(c.execute(sqlstatement, (receivers,)))
        if not message or len(u) < 1:
            return f'ERROR: missing sender or message'
        stmt = f"INSERT INTO messages (sender, reciever, message, sendtime) values (?, ?, ?, DATETIME());"
        conn.execute(stmt, (currentUser, receivers, enc.encryptMsg(message)))
        return f'ok'
    except Error as e:
        return f'ERROR: {e}'


@app.route('/inbox', methods=['GET', 'POST'])
@login_required
def inbox():
    return render_template('./inbox.html')


@app.route('/reply', methods=['GET', 'POST'])
@login_required
def reply():
    return render_template('./reply.html')


@app.route('/chooseMsg', methods=['GET', 'POST'])
def chooseMsg():
    currentUser = flask_login.current_user.id
    try:
        stmt = "SELECT message FROM messages WHERE reciever == ?;"
        messages = list(c.execute(stmt, (currentUser,)))
        if len(messages) < 1:
            return "You have received no messages"
        c.close()
        return messages
    except Error as e:
        return f'Error: {e}'


@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender': escape(row[0]), 'message': escape(row[1])})
        return {'data': anns}
    except Error as e:
        return {'error': f'{e}'}


@app.get('/coffee/')
def nocoffee():
    abort(418)


@app.route('/coffee/', methods=['POST', 'PUT'])
def gotcoffee():
    return "Thanks!"


@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp


try:
    loggerConn = apsw.Connection('databases/applogger.db')
    conn = apsw.Connection('databases/tiny.db')
    loggerc = loggerConn.cursor()
    c = conn.cursor()
    loggerc.execute('''CREATE TABLE IF NOT EXISTS userloggs (
        id integer PRIMARY KEY,
        username BLOB,
        password BLOB,
        reason TEXT NOT NULL,
        sendtime DATETIME CURRENT_TIMESTAMP);''')
    loggerc.execute('''CREATE TABLE IF NOT EXISTS messageloggs (
        id integer PRIMARY KEY,
        sender BLOB NOT NULL,
        receiver BLOB NOT NULL,
        message BLOB NOT NULL,
        reason TEXT NOT NULL,
        sendtime DATETIME CURRENT_TIMESTAMP);''')
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        reciever TEXT NOT NULL,
        message BLOB NOT NULL,
        sendtime DATETIME CURRENT_TIMESTAMP);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS users (
        username TEXT PRIMARY KEY,
        password TEXT NOT NULL,
        token TEXT);''')
except Error as e:
    print(e)
    sys.exit(1)
